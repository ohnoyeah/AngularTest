/*global angular  */

var herokuAPI = "https://heroku-newindex.c9users.io"
//var herokuAPI = "https://ohnoyeah.herokuapp.com/"

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute']).run(function($rootScope,$http){
  $http.get(herokuAPI+"/users").then(function(response){
    $rootScope.user=response.data
  })
    
})

/* the config function takes an array. */
myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/search', {
            templateUrl: 'views/search.html',
            controller: 'searchController'
		})
		.when('/login', {
            templateUrl: 'views/login.html',
            controller: 'userController'
		})
		.when('/register',{
		        templateUrl:'views/register.html',
		        controller: 'userController'
		})
		.when('/logout', {
            templateUrl: 'views/login.html',
            controller: 'userController'
		})
    .when('/detail/:id', {
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/bookmark', {
		  templateUrl: 'views/bookmarkList.html',
      controller: 'bookmarkController'
		})
		.otherwise({
		  redirectTo: 'search'
		})
	}])


myApp.controller('searchController', function($scope, $http) {
  $scope.message = 'This is the search screen'
  


  $scope.search = function() {
    console.log('search()')
      var search = $scope.keyword
      console.log(search)
      $http.get(herokuAPI+"/books?keyword="+search).then(function(response) {
        console.log(response)
        $scope.books = response.data
        $scope.keyword = ''
      })
    
  }
})

myApp.controller('userController',function($scope,$rootScope, $http){
      $scope.login = function(){
      var loginInfo = "email=" + $scope.email+"&password="+$scope.password
      $http({
            method: 'POST',
            url: herokuAPI+'/users/login',
            data: loginInfo,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
          
          var data = response.data
          console.log("login:"+data)
          if(data["null"]){
            alert("Invalid Email/Password")
          }
          else{
            
            alert("Welcome "+data.displayName)
            $rootScope.user = data
            window.location.replace("#!search");
          }
          
      }).catch(function(reason){
        alert("Invalid Email/Password")
        console.log(reason)
      })
    }
    
    $scope.logout=function(){
        $http.get(herokuAPI+"/users/logout").then(function(response) {
        console.log(response.data["result"])
        $rootScope.user = null
      })
    }
    
    $scope.register = function(){
      var regisInfo = "name="+$scope.name+"&email=" + $scope.email+"&password="+$scope.password
      $http({
            method: 'POST',
            url: herokuAPI+'/users/register',
            data: regisInfo,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
          
          
          var data = response.data
          console.log(response)
            if(data["result"]=="fail"){
              alert(data["data"])
            }
            else{
               alert("Welcome "+data["user"].displayName)
                $rootScope.user = data["user"]
                window.location.replace("#!search");
            }
            
           
          
          
      }).catch(function(reason){
        console.log(reason)
      })
    }
    
})

/*myApp.controller('loginController',function($scope,$rootScope, $http) {
    
    
    $scope.login = function(){
      var loginInfo = "email=" + $scope.email+"&password="+$scope.password
      $http({
            method: 'POST',
            url: herokuAPI+'/users/login',
            data: loginInfo,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
          
          var data = response.data
          console.log("login:"+data)
          if(data["null"]){
            alert("Invalid Email/Password")
          }
          else{
            
            alert("Welcome "+data.displayName)
            $rootScope.user = data
            window.location.replace("#!search");
          }
          
      }).catch(function(reason){
        alert("Invalid Email/Password")
        console.log(reason)
      })
    }

})

myApp.controller('logoutController',function($rootScope,$http) {
    $http.get(herokuAPI+"/users/logout").then(function(response) {
        console.log(response.data["result"])
        $rootScope.user = null
      })
})
*/



myApp.controller('bookmarkController',function($scope,$rootScope,$http,$location,$route){
    var user = $scope.user
    
      $http.get(herokuAPI+"/bookmark").then(function(response) {
        $scope.bookmarks = response.data
        console.log(response.data)
      })
    

    $scope.addBookmark = function(index){
      console.log(user)
      if(!user){
        alert("Sign in to use this feature")
        return
      }
      $http({
            method: 'POST',
            url: herokuAPI+'/bookmark',
            data: "bookId="+$scope.books[index].id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
        console.log($scope.books[index].id)
        console.log("add bookmark")
        alert(response.data["result"]+" to add bookmark")
      })
    }
    $scope.removeBookmark = function(index){
        $http({
            method: 'DELETE',
            url: herokuAPI+'/bookmark',
            data: "bookId="+$scope.bookmarks[index].bookId,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
        console.log($scope.bookmarks[index].bookId)
        console.log("remove bookmark")
        console.log(response)
        alert(response.data["result"]+" to delete bookmark")
        $location.path("/bookmark")
        $route.reload()
      })
    }
})


myApp.controller('detailController', function($scope, $routeParams) {
  $scope.message = 'This is the detail screen'
  $scope.id = $routeParams.id
  $scope.addToFavourites = function(id) {
    console.log('adding: '+id+' to favourites.')
    localStorage.setItem(id, id)
  }
})
 
myApp.controller('favouritesController', function($scope) {
  console.log('fav controller')
  $scope.message = 'This is the favourites screen'
  $scope.delete = function(id) {
    console.log('deleting id '+id)
  }
  var init = function() {
    console.log('getting books')
    var items = Array()
    for (var a in localStorage) {
      items.push(localStorage[a])
    }
    console.log(items)
    $scope.books = items
  }
  init()
})
